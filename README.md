# Docker Workshop
Lab 06: Building more complex images

---

## Instructions

 - Ensure you are in the previous created folder path (lab-05):
```
pwd
```

 - Create a file named "**index.html**" next to the Dockerfile with the content below:
```
echo "<h1>Python App</h1>" > index.html
```

 - Check your current directory before you proceed:
```
ls -l

total 2
-rw-r--r-- 1 docker 1049089 123 Jun 13 21:37 Dockerfile
-rw-r--r-- 1 docker 1049089  19 Jun 13 21:54 index.html
```

 - Edit the **Dockerfile** to expose the port 5000
```
EXPOSE 5000
```
 - Like so:
```
FROM selaworkshops/alpine_new:3.4
RUN apk add --no-cache python
CMD python -m SimpleHTTPServer 5000
EXPOSE 5000
```

 - Replace the CMD instruction for ENTRYPOINT to avoid override the command from the docker run command:
```
ENTRYPOINT python -m SimpleHTTPServer 5000
```
- Like so:
```
FROM selaworkshops/alpine_new:3.4
RUN apk add --no-cache python
ENTRYPOINT python -m SimpleHTTPServer 5000
EXPOSE 5000
```

 - Add a build argument to set the application port during the build:
```
ARG port=5000
EXPOSE $port
```
- Like so:
```
FROM selaworkshops/alpine_new:3.4
RUN apk add --no-cache python
ARG port=5000
ENTRYPOINT python -m SimpleHTTPServer $port
EXPOSE $port
```

 - Add a environment variable to override the build argument at runtime:
```
ENV port=$port
```
- Like so:
```
FROM selaworkshops/alpine_new:3.4
RUN apk add --no-cache python
ARG port=5000
ENV port=$port
ENTRYPOINT python -m SimpleHTTPServer $port
EXPOSE $port
```

 - Set the app directory as the container working directory:
```
WORKDIR /app
```
- Like so:
```
FROM selaworkshops/alpine_new:3.4
RUN apk add --no-cache python
ARG port=5000
ENV port=$port
WORKDIR /app
ENTRYPOINT python -m SimpleHTTPServer $port
EXPOSE $port
```

 - Copy the index file to the container using the COPY instruction:
```
COPY index.html /app/
```
- Like so:
```
FROM selaworkshops/alpine_new:3.4
RUN apk add --no-cache python
ARG port=5000
ENV port=$port
WORKDIR /app
COPY index.html /app/
ENTRYPOINT python -m SimpleHTTPServer $port
EXPOSE $port
```

 - Build the new image passing a build argument:
```
docker build --build-arg port=5001 -t python-app:2.0 .
```

 - Run the created image without passing environment variables:
```
docker run -it --name app1 -p 5001:5001 python-app:2.0
```

 - Browse to the application:
```
http://<your-server-ip>:5001
```

 - Exit from the running container:
```
(Ctrl + C)
```

 - Run the created image passing the port 5002 as parameter:
```
docker run -it --name app2 -p 5002:5002 -e "port=5002" python-app:2.0
```

 - Browse to the application:
```
http://<your-server-ip>:5002
```

 - Exit from the running container:
```
(Ctrl + C)
```

## Cleanup

 - Remove used containers:
```
docker rm -f app1 app2
```

